package ru.dgaribov.revoluttest;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import ru.dgaribov.revoluttest.controller.MainController;
import ru.dgaribov.revoluttest.dao.AccountDAO;

import static ru.dgaribov.revoluttest.util.DbUtil.clearDB;

/**
 * Starts application, setting up Jetty server and DB
 *
 * @author David Garibov
 */
public class Application {

    private Server jettyServer;

    public static void main(String[] args) throws Exception {
        Application application = new Application();
        application.runApp();
    }

    void runApp() throws Exception {
        initServer();
        startServer();
        prepareDB();
    }


    private void initServer() {
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");

        jettyServer = new Server(8181);
        jettyServer.setHandler(context);
        initJersey(context);

    }

    private void startServer() throws Exception {
        try {
            jettyServer.start();
            new Thread(() -> {
                try {
                    jettyServer.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        } catch (Exception e) {
            jettyServer.stop();
            jettyServer.destroy();
        }
    }

    private void initJersey(ServletContextHandler context) {
        ServletHolder jerseyServlet = context.addServlet(
                org.glassfish.jersey.servlet.ServletContainer.class, "/*");


        jerseyServlet.setInitOrder(0);


        jerseyServlet.setInitParameter(
                "jersey.config.server.provider.classnames",
                MainController.class.getCanonicalName());

        jerseyServlet.setInitParameter("com.sun.jersey.api.json.POJOMappingFeature", "true");

    }

    void stopServer() throws Exception {
        clearDB();
        jettyServer.stop();
    }


    /**
     * Initialising DB and filling it with test data
     */
    private void prepareDB() {
        new AccountDAO().createTestAccounts();

    }


}
