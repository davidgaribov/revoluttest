package ru.dgaribov.revoluttest.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Entity of transaction, money transfer would be running within
 *
 * @author David Garibov
 */
@Entity
@Data
public class MoneyTransaction {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @JoinColumn
    @ManyToOne
    private Account accountFrom;

    @JoinColumn
    @ManyToOne
    private Account accountTo;

    @Column
    private BigDecimal moneyAmount;

    @Column
    private boolean committed;


    public Integer getId() {
        return id;
    }
}
