package ru.dgaribov.revoluttest.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * Account entity to persist in database
 *
 * @author David Garibov
 * Created on 27/04/2017
 */

@Entity
@Data
public class Account {
    @Id
    private Integer id;

    @Column
    private BigDecimal moneyAmount;


    public boolean hasSufficientBalance(BigDecimal amountToTransfer) {
        return moneyAmount.compareTo(amountToTransfer) >= 0;
    }

    public void debit(BigDecimal moneyAmount) {
        this.moneyAmount = this.moneyAmount.add(moneyAmount);
    }

    public void credit(BigDecimal moneyAmount) {
        this.moneyAmount = this.moneyAmount.subtract(moneyAmount);
    }
}
