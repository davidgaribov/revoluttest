package ru.dgaribov.revoluttest.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Class for Hibernate session obtain
 *
 * @author David Garibov
 */
public class SessionProvider {
    private static SessionProvider entity;
    private SessionFactory sessionFactory;

    public static SessionProvider get() {
        if (entity == null) entity = new SessionProvider();
        return entity;
    }

    private SessionProvider() {
        sessionFactory = new Configuration().configure()
                .buildSessionFactory();
    }

    public Session getSession() {
        return sessionFactory.openSession();
    }
}
