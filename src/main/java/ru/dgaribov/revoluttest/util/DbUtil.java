package ru.dgaribov.revoluttest.util;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class DbUtil {
    public static void clearDB() {
        Session session = SessionProvider.get().getSession();
        Transaction tx = session.beginTransaction();
        String hql = String.format("delete from %s", "Account");
        Query query = session.createQuery(hql);
        query.executeUpdate();
        hql = String.format("delete from %s", "MoneyTransaction");
        query = session.createQuery(hql);
        query.executeUpdate();
        tx.commit();
    }
}
