package ru.dgaribov.revoluttest.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Data transfer object containing information of money transfer
 *
 * @author David Garibov
 */
@Data
public class TransactionDTO {

    private int transactionId;
    private Integer accountIdFrom;
    private Integer accountIdTo;
    private BigDecimal amount;

}
