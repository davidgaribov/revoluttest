package ru.dgaribov.revoluttest.service;

import org.hibernate.Session;
import ru.dgaribov.revoluttest.dao.TransactionDAO;
import ru.dgaribov.revoluttest.dto.TransactionDTO;
import ru.dgaribov.revoluttest.entity.Account;
import ru.dgaribov.revoluttest.entity.MoneyTransaction;
import ru.dgaribov.revoluttest.util.SessionProvider;

import javax.persistence.LockModeType;
import java.math.BigDecimal;

/**
 * Transfers money from one account to another
 *
 * @author David Garibov
 */
public class MoneyTransferService {


    private SessionProvider sessionProvider = SessionProvider.get();

    private TransactionDAO transactionDAO = new TransactionDAO();

    /**
     *
     * @return a new money transfer transaction
     */
    public Integer createTransaction() {
        return transactionDAO.create();
    }


    /**
     * Checks submitted data before money transfer
     *
     * @param transactionDTO transaction data transfer object containing info for money transfer
     * @throws IllegalArgumentException if validation fails
     */
    private void preValidate(TransactionDTO transactionDTO) throws IllegalArgumentException {

        if (transactionDTO == null)
            throw new IllegalArgumentException("MoneyTransaction transfer object was not submitted\"");
        if (transactionDTO.getTransactionId() < 1) throw new IllegalArgumentException("bad transaction id");


        if (transactionDTO.getAmount() == null || transactionDTO.getAmount().compareTo(BigDecimal.ZERO) < 1)
            throw new IllegalArgumentException("Invalid money amount was submitted");


        if (transactionDTO.getAccountIdFrom().equals(transactionDTO.getAccountIdTo())) {
            throw new IllegalArgumentException("Cannot transfer from an account to itself");
        }
    }


    /**
     * Transfers money from one account to another
     */
    public void transferMoney(TransactionDTO transactionDTO) throws IllegalArgumentException {

        preValidate(transactionDTO);

        Session session = sessionProvider.getSession();
        session.getTransaction().begin();

        MoneyTransaction moneyTransaction = validateAndGetMoneyTransaction(transactionDTO.getTransactionId(), session);

        Account accountFrom = validateAndGetAccountFrom(transactionDTO.getAccountIdFrom(), transactionDTO.getAmount(), session);
        Account accountTo = validateAndGetAccountTo(transactionDTO.getAccountIdTo(), session);

        accountFrom.credit(transactionDTO.getAmount());
        accountTo.debit(transactionDTO.getAmount());

        moneyTransaction.setCommitted(true);

        session.persist(accountFrom);
        session.persist(accountTo);
        session.persist(moneyTransaction);

        session.getTransaction().commit();
        session.close();
    }

    private MoneyTransaction validateAndGetMoneyTransaction(Integer transactionId, Session session) throws IllegalArgumentException {
        MoneyTransaction transaction = session.find(MoneyTransaction.class, transactionId, LockModeType.PESSIMISTIC_WRITE);
        if (transaction == null) {
            session.getTransaction().rollback();
            throw new IllegalArgumentException("No transaction with given id was found");
        }
        if (transaction.isCommitted()) {
            session.getTransaction().rollback();
            throw new IllegalArgumentException("This money transfer transaction has already bean committed");
        }
        return transaction;
    }


    private Account validateAndGetAccountFrom(Integer accountFromId, BigDecimal moneyToTransfer, Session session) throws IllegalArgumentException {
        Account accountFrom = session.find(Account.class, accountFromId, LockModeType.PESSIMISTIC_WRITE);
        if (accountFrom == null) {
            session.getTransaction().rollback();
            throw new IllegalArgumentException("No from-account was found for given id");
        }
        if (!accountFrom.hasSufficientBalance(moneyToTransfer)) {
            session.getTransaction().rollback();
            throw new IllegalArgumentException("From-account has not got sufficient balance");
        }
        return accountFrom;
    }

    private Account validateAndGetAccountTo(Integer accountToId, Session session) {
        Account accountTo = session.find(Account.class, accountToId, LockModeType.PESSIMISTIC_WRITE);
        if (accountTo == null) {
            session.getTransaction().rollback();
            throw new IllegalArgumentException("No to-account was found for given id");
        }
        return accountTo;
    }
}
