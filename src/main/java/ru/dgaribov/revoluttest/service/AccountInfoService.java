package ru.dgaribov.revoluttest.service;

import ru.dgaribov.revoluttest.dao.AccountDAO;
import ru.dgaribov.revoluttest.entity.Account;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class AccountInfoService {
    private AccountDAO accountDAO = new AccountDAO();

    public BigDecimal getAccountBalance(Integer accountId) throws IllegalArgumentException {
        Account account = accountDAO.get(accountId);
        if (account == null) throw new IllegalArgumentException("No account was found for given id");

        BigDecimal money = account.getMoneyAmount();
        return money.setScale(2, RoundingMode.HALF_UP);
    }
}
