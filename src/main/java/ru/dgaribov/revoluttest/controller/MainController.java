package ru.dgaribov.revoluttest.controller;

import ru.dgaribov.revoluttest.dto.TransactionDTO;
import ru.dgaribov.revoluttest.service.AccountInfoService;
import ru.dgaribov.revoluttest.service.MoneyTransferService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST controller for external HTTP requests
 *
 * @author David Garibov
 */
@Path("/")
public class MainController {


    private MoneyTransferService moneyTransferService = new MoneyTransferService();
    private AccountInfoService accountInfoService = new AccountInfoService();

    /**
     * @param accountId ---
     * @return Returns money amount of given account
     */
    @GET
    @Path("/getAccountBalance/")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getAccountBalance(
            @QueryParam("accountId") Integer accountId
    ) {

        try {
            return Response.ok(accountInfoService
                    .getAccountBalance(accountId)).build();
        } catch (IllegalArgumentException iae) {
            return Response.status(404).entity(iae.getMessage()).build();
        } catch (Exception ex) {
            return Response.status(500).entity(ex.getMessage()).build();
        }


    }

    /**
     * Creates a transaction entity, money transfer will be running within
     *
     * @return transaction
     */
    @POST
    @Path("/createTransaction")
    public Response createTransaction() {
        try {
            return Response.ok(moneyTransferService.createTransaction()).build();
        } catch (Exception ex) {
            return Response.status(500).entity(ex.getMessage()).build();
        }


    }

    /**
     * Transfers money from one account to another within transaction
     *
     * @param transactionDTO all info about data transfer
     * @return HTTP response
     */
    @POST
    @Path("/transferMoney")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response moneyTransfer(
            TransactionDTO transactionDTO
    ) {
        try {
            moneyTransferService.transferMoney(transactionDTO);
        } catch (IllegalArgumentException iae) {
            return Response.status(404).entity(iae.getMessage()).build();
        } catch (Exception ex) {
            return Response.status(500).entity(ex.getMessage()).build();
        }
        return Response.ok().build();
    }

}