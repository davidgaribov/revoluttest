package ru.dgaribov.revoluttest.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;
import ru.dgaribov.revoluttest.entity.Account;
import ru.dgaribov.revoluttest.util.SessionProvider;

import java.math.BigDecimal;
import java.util.List;


/**
 * DAO for account entity
 *
 * @author David Garibov
 */
public class AccountDAO {
    public Account get(Integer id) {
        Session session = SessionProvider.get().getSession();
        Query<Account> query = session.createQuery("from Account WHERE id = :id");
        query.setParameter("id", id);
        List<Account> accountList = query.list();
        session.close();
        if (accountList == null || accountList.size() == 0) return null;
        else return accountList.get(0);
    }

    public void createTestAccounts() {
        Session session = SessionProvider.get().getSession();
        Account account1 = new Account();
        account1.setId(1);
        account1.setMoneyAmount(BigDecimal.TEN);
        Account account2 = new Account();
        account2.setId(2);
        account2.setMoneyAmount(BigDecimal.TEN);
        session.beginTransaction();
        session.save(account1);
        session.save(account2);
        session.getTransaction().commit();
        session.close();
    }
}
