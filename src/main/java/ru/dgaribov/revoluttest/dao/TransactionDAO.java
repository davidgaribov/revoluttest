package ru.dgaribov.revoluttest.dao;

import org.hibernate.Session;
import ru.dgaribov.revoluttest.entity.MoneyTransaction;
import ru.dgaribov.revoluttest.util.SessionProvider;

/**
 * DAO for transaction entity
 *
 * @author David Garibov
 */
public class TransactionDAO {
    public Integer create() {
        MoneyTransaction transaction = new MoneyTransaction();
        Session session = SessionProvider.get().getSession();
        session.beginTransaction();
        session.save(transaction);
        session.getTransaction().commit();
        session.close();
        return transaction.getId();
    }
}
