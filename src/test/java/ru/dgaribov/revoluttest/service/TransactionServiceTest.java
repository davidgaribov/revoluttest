package ru.dgaribov.revoluttest.service;

import org.junit.Assert;
import org.junit.Test;
import ru.dgaribov.revoluttest.dao.AccountDAO;
import ru.dgaribov.revoluttest.dao.TransactionDAO;
import ru.dgaribov.revoluttest.dto.TransactionDTO;
import ru.dgaribov.revoluttest.entity.Account;

import java.math.BigDecimal;

public class TransactionServiceTest {

    @Test
    public void moneyTransferSuccessfulTest() {
        AccountDAO accountDAO = new AccountDAO();
        accountDAO.createTestAccounts();

        TransactionDAO transactionDAO = new TransactionDAO();
        int transactionId = transactionDAO.create();

        MoneyTransferService moneyTransferService = new MoneyTransferService();
        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setAccountIdFrom(1);
        transactionDTO.setAccountIdTo(2);
        transactionDTO.setAmount(new BigDecimal("5.00"));
        transactionDTO.setTransactionId(transactionId);
        moneyTransferService.transferMoney(transactionDTO);

        Account account1 = accountDAO.get(1);
        Account account2 = accountDAO.get(2);

        Assert.assertEquals(new BigDecimal("5.00"), account1.getMoneyAmount());
        Assert.assertEquals(new BigDecimal("15.00"), account2.getMoneyAmount());
    }

    @Test(expected = IllegalArgumentException.class)
    public void moneyTransferBadTransactionTest() {
        MoneyTransferService moneyTransferService = new MoneyTransferService();
        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setAccountIdFrom(1);
        transactionDTO.setAccountIdTo(2);
        transactionDTO.setAmount(new BigDecimal("5.00"));
        transactionDTO.setTransactionId(1);
        moneyTransferService.transferMoney(transactionDTO);
    }
}
