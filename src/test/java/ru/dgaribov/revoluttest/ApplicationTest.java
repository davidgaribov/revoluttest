package ru.dgaribov.revoluttest;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author David Garibov
 */
public class ApplicationTest {

    private Application application = new Application();

    @Before
    public void startServer() throws Exception {
        application.runApp();
    }


    /**
     * Tests that application is running and there is $10.00 on each test account
     */
    @Test
    public void testInitialAccountMoneyAmount() throws Exception {

        BigDecimal account1MoneyAmountExpected = new BigDecimal("10.00");
        BigDecimal account1MoneyAmountActual = getAccountMoneyAmount(1);
        assertEquals(account1MoneyAmountExpected, account1MoneyAmountActual);
        BigDecimal account2MoneyAmountActual = getAccountMoneyAmount(2);
        assertEquals(account1MoneyAmountExpected, account2MoneyAmountActual);
    }


    /**
     * Tests that transaction is being created successfully
     */
    @Test
    public void testTransactionCreation() throws Exception {
        int transactionId = createTransaction();
        assertTrue(transactionId > 0);
    }

    /**
     * Tests that there would be no money transfer for transaction that does not exist
     */
    @Test
    public void testMoneyTransferNotExecutingWithBadTransaction() throws Exception {
        Integer httpCode = transferMoneyFromAccount1ToAccount2(new BigDecimal("5.55"), 10);
        Integer expectedCode = 404;
        assertEquals(expectedCode, httpCode);
    }

    /**
     * Tests that there would be no money transfer if withdrawal account does not have enough money
     */
    @Test
    public void testTooMuchMoneyWouldNotBeTransferred() throws Exception {
        int transactionId = createTransaction();

        Integer httpCode = transferMoneyFromAccount1ToAccount2(new BigDecimal(1000), transactionId);
        Integer expectedCode = 404;
        assertEquals(expectedCode, httpCode);
        testInitialAccountMoneyAmount();
    }

    /**
     * Tests that:
     * 1) Money would be successfully tranferred from account 1 to account 2
     * 3) We could not perform another money transfer within same transaction, cause it is already committed
     */
    @Test
    public void testSuccessfulMoneyTransferAndNotSuccessfulRepeatInSameTransaction() throws Exception {

        int transactionId = createTransaction();
        Integer transferAnswerHttpCode = transferMoneyFromAccount1ToAccount2(new BigDecimal("5.55"), transactionId);
        Integer expectedCode = 200;
        assertEquals(expectedCode, transferAnswerHttpCode);
        BigDecimal account1MoneyAmountExpected = new BigDecimal("4.45");
        BigDecimal account1MoneyAmountActual = getAccountMoneyAmount(1);
        assertEquals(account1MoneyAmountExpected, account1MoneyAmountActual);
        BigDecimal account2MoneyAmountExpected = new BigDecimal("15.55");
        BigDecimal account2MoneyAmountActual = getAccountMoneyAmount(2);
        assertEquals(account2MoneyAmountExpected, account2MoneyAmountActual);

        transferAnswerHttpCode = transferMoneyFromAccount1ToAccount2(new BigDecimal("5.55"), transactionId);
        expectedCode = 404;

        assertEquals(expectedCode, transferAnswerHttpCode);
    }

    @After
    public void stopServer() throws Exception {
        application.stopServer();
    }

    private BigDecimal getAccountMoneyAmount(int accountId) throws Exception {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet("http://localhost:8181/getAccountBalance/?accountId=" + accountId);
        HttpResponse response = client.execute(request);
        BufferedReader rd = new BufferedReader
                (new InputStreamReader(response.getEntity().getContent()));
        String accountMoneyAmount = rd.readLine();
        return new BigDecimal(accountMoneyAmount);
    }

    private int createTransaction() throws Exception {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost transactionRequest = new HttpPost("http://localhost:8181/createTransaction");
        HttpResponse transactionResponse = client.execute(transactionRequest);
        BufferedReader rd = new BufferedReader
                (new InputStreamReader(transactionResponse.getEntity().getContent()));
        String transactionId = rd.readLine();
        return Integer.parseInt(transactionId);
    }

    private int transferMoneyFromAccount1ToAccount2(BigDecimal moneyAmount, int transactionId) throws Exception {
        HttpClient client = HttpClientBuilder.create().build();

        HttpPost request = new HttpPost("http://localhost:8181/transferMoney");
        request.setHeader("Content-Type", "application/json");
        request.setHeader("Charset", "utf-8");
        String content = "{\"accountIdFrom\": \"1\", \"accountIdTo\": \"2\", \"amount\":\"" + moneyAmount + "\", " +
                "\"transactionId\" : \"" + transactionId + "\"}";
        BasicHttpEntity requestEntity = new BasicHttpEntity();
        requestEntity.setContent(new ByteArrayInputStream(content.getBytes()));
        request.setEntity(requestEntity);
        HttpResponse response = client.execute(request);
        return response.getStatusLine().getStatusCode();
    }
}
