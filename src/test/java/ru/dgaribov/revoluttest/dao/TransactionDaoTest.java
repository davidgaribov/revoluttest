package ru.dgaribov.revoluttest.dao;


import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.dgaribov.revoluttest.util.DbUtil;

public class TransactionDaoTest {

    @Test
    public void newMoneyTransactionSuccessfulCreationTest() {
        TransactionDAO transactionDAO = new TransactionDAO();
        Integer expectedId = 1;
        Assert.assertEquals(expectedId, transactionDAO.create());
    }

    @After
    public void clearDB() {
        DbUtil.clearDB();
    }
}
