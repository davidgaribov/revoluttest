package ru.dgaribov.revoluttest.dao;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.dgaribov.revoluttest.entity.Account;
import ru.dgaribov.revoluttest.util.DbUtil;

import java.math.BigDecimal;

public class AccountDaoTest {

    @Test
    public void testAccountsSuccessfullyCreatedTest() {
        AccountDAO accountDAO = new AccountDAO();
        accountDAO.createTestAccounts();
        BigDecimal testMoneyAmount = new BigDecimal("10.00");
        Account account1 = accountDAO.get(1);
        Account account2 = accountDAO.get(2);
        Assert.assertEquals(testMoneyAmount, account1.getMoneyAmount());
        Assert.assertEquals(testMoneyAmount, account2.getMoneyAmount());
    }

    @After
    public void clearDB() {
        DbUtil.clearDB();
    }
}
