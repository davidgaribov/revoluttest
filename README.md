# README #

This Java app was developed by David Garibov as a job test for Revolut ltd.
<br /><br />
You can build standalone application using gradle jar task.
revoluttest.jar - already assembled application, which starts Jetty server on port 8181.

<br /><br />
API:
<br /><br />
GET: /getAccountBalance?accountId={accountId} - returns money amount for given account.
<br />
POST: /createTransaction - creates transaction, money transfer would be running within.
<br />
POST: /transferMoney - executes given transaction by submitted JSON body. 
Example: {"accountIdFrom": "1", "accountIdTo": "2", "amount":"3.55", "transactionId": "1"}